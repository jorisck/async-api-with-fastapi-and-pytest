from app.api import notes, ping
from app.db import database, engine, metadata
from fastapi import FastAPI

metadata.create_all(engine)

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(ping.router)
app.include_router(notes.router, prefix="/notes", tags=["notes"])

#https://testdriven.io/blog/fastapi-crud/
#Background Tasks: https://fastapi.tiangolo.com/tutorial/background-tasks/
#auth : https://fastapi.tiangolo.com/tutorial/security/simple-oauth2/
#Gunicorn: https://www.uvicorn.org/#running-with-gunicorn
#Deployment: https://fastapi.tiangolo.com/deployment/